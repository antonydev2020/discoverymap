//
//  CLLocationMangerMock.swift
//  DiscoveryMapTests
//
//  Created by Antony Alexander Mylvaganam on 1/18/22.
//

import CoreLocation
import Foundation

protocol LocationManager {
    
    var location: CLLocation? { get }
    var clDelegate: CLLocationManagerDelegate? { get set }
    
    func getAuthorizationStatus() -> CLAuthorizationStatus
    func isLocationServiceEnabled() -> Bool
    
    func requestWhenInUseAuthorization()
    func requestAlwaysAuthorization()
    func startUpdatingLocaiton()
    func stopUpdatingLocaiton()
}

class CLLocationMangerMock: LocationManager {
    
    weak var clDelegate: CLLocationManagerDelegate?
    var location: CLLocation? = CLLocation(latitude: 37.8199, longitude: 122.4783) // Golden gate bridge
    
    func requestWhenInUseAuthorization() { }
    func requestAlwaysAuthorization() { }
    
    func startUpdatingLocaiton() { }
    func stopUpdatingLocaiton() { }
    
    func getAuthorizationStatus() -> CLAuthorizationStatus {
        return .authorizedWhenInUse
    }
    
    func isLocationServiceEnabled() -> Bool {
        return true
    }
}

extension CLLocationManager: LocationManager {
    var clDelegate: CLLocationManagerDelegate? {
        get {
            guard let delegate = delegate as? CLLocationManagerDelegate else { return nil }
            return delegate
        }
        set {
            delegate = newValue as? CLLocationManagerDelegate
        }
    }
    
    func startUpdatingLocaiton() {
        <#code#>
    }
    
    func stopUpdatingLocaiton() {
        <#code#>
    }
    
    func getAuthorizationStatus() -> CLAuthorizationStatus {
        return CLLocationManager.authorizationStatus()
    }
    
    func isLocationServiceEnabled() -> Bool {
        return CLLocationManager.locationServicesEnabled()
    }
}
