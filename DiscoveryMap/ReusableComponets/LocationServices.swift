//
//  LocationServices.swift
//  DiscoveryMap
//
//  Created by Antony Alexander Mylvaganam on 1/6/22.
//

import CoreLocation
import Foundation

protocol LocationServiceDelegate: AnyObject {
    func didFetch(location: CLLocation)
    func didFail(with error: LocationServices.LocationError)
}

class LocationServices: NSObject, CLLocationManagerDelegate {
    enum LocationError: Error {
        case noAccess
        case custom(Error)
    }
    
    private var locationProvider: CLLocationManager
    weak var delegate: LocationServiceDelegate?
    
    init(locationProvider: CLLocationManager) {
        self.locationProvider = locationProvider
        super.init()
        self.locationProvider.desiredAccuracy = kCLLocationAccuracyBest
        self.locationProvider.delegate = self
    }
    
    func fetchUserLocation() {
        checkAuthorizationIfRequired()
    }
    
    func stopLocationUpdates() {
        locationProvider.stopUpdatingHeading()
    }
    
    private func checkAuthorizationIfRequired() {
        let status: CLAuthorizationStatus
        if #available(iOS 14.0, *) {
            status = locationProvider.authorizationStatus
        } else {
            status = CLLocationManager.authorizationStatus()
        }
        
        switch status {
        case .notDetermined:
            locationProvider.requestWhenInUseAuthorization()
            locationProvider.requestAlwaysAuthorization()
            
        case .restricted, .denied:
            self.delegate?.didFail(with: .noAccess)
            
        case .authorizedAlways, .authorizedWhenInUse:
            locationProvider.requestLocation()
            
        @unknown default:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted, .denied:
            self.delegate?.didFail(with: .noAccess)
            
        case .authorizedAlways, .authorizedWhenInUse:
            locationProvider.requestLocation()
            
        case .notDetermined:
            break
            
        @unknown default:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            delegate?.didFetch(location: location)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        delegate?.didFail(with: .custom(error))
    }
}
