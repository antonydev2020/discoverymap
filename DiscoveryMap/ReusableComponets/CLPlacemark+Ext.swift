//
//  CLPlacemark+Ext.swift
//  DiscoveryMap
//
//  Created by Antony Alexander Mylvaganam on 1/7/22.
//

import CoreLocation
import Foundation
import MapKit

extension CLPlacemark {
    var formattedAddress: String {
        [
            subThoroughfare, thoroughfare, locality, administrativeArea, postalCode, country
        ]
            .compactMap { $0 }
            .filter { !$0.isEmpty }
            .joined(separator: ", ")
    }
}

extension MKAnnotation {
    func openInMap() {
        let span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
        let region = MKCoordinateRegion(center: coordinate, span: span)
        
        let options = [
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinate: region.center),
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinateSpan: region.span)
        ]

        let placeMark = MKPlacemark(coordinate: coordinate)
        let mapItem = MKMapItem(placemark: placeMark)
        mapItem.name = title ?? ""
        mapItem.openInMaps(launchOptions: options)
    }

    func openInDialer() {
        if let phoneUrl = URL(string: "tel: //\(subtitle ?? "")"), UIApplication.shared.canOpenURL(phoneUrl) {
            UIApplication.shared.open(phoneUrl)
        }
    }
}
