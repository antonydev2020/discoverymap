//
//  ConfigurableAnnotation.swift
//  DiscoveryMap
//
//  Created by Antony Alexander Mylvaganam on 1/13/22.
//

import Foundation
import MapKit

enum AnnotationType {
    case places
    case search
}

protocol ConfigurableAnnotation: MKAnnotation {
    var type: AnnotationType { get }
    var identifier: String { get }
    
    var title: String? { get }
    var info: String? { get }
    var phone: String? { get }
    var image: String? { get }
    var zipCode: String? { get }
}
