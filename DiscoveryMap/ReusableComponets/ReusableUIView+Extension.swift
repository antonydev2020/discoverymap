//
//  ReusableUIView+Extension.swift
//  DiscoveryMap
//
//  Created by Antony Alexander Mylvaganam on 1/12/22.
//

import Foundation
import UIKit

protocol ReusableUIView {
    static var reuseIdentifier: String { get }
}

extension ReusableUIView where Self: UIView {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

extension UIView {
    class func loadFromNib(name: String? = nil) -> Self {
        let name = name ?? "\(Self.self)"
        guard let nib = Bundle.main.loadNibNamed(name, owner: nil, options: nil) else {
            fatalError("missing nib name, please check")
        }
        guard let view = nib.first as? Self else {
            fatalError("view of type \(Self.self) not found in \(nib)")
        }
        return view
    }
}
