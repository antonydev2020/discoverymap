//
//  GoogleViewController.swift
//  DiscoveryMap
//
//  Created by Antony Alexander Mylvaganam on 1/6/22.
//

import GoogleMaps
import UIKit

// swiftlint:disable unused_closure_parameter

class GoogleViewController: UIViewController {
    
    @IBOutlet private weak var googleMap: GMSMapView! {
        didSet {
            googleMap.delegate = self
        }
    }
    @IBOutlet private weak var placesLabel: UILabel!
    @IBOutlet private weak var mapPinImage: UIImageView!
    @IBOutlet private weak var mapPinVerticalConstraint: NSLayoutConstraint!
    
    private var locationManager = CLLocationManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestLocation()
            googleMap.isMyLocationEnabled = true
            googleMap.settings.myLocationButton = true
        } else {
            locationManager.requestWhenInUseAuthorization()
        }

        // Do any additional setup after loading the view.
    }
    
    func reverseGeocode(coordinate: CLLocationCoordinate2D) {
      let geocoder = GMSGeocoder()
        
        let labelHeight = self.placesLabel.intrinsicContentSize.height
        let topInset = self.view.safeAreaInsets.top
        self.googleMap.padding = UIEdgeInsets(top: topInset,
                                              left: 0,
                                              bottom: labelHeight,
                                              right: 0)

      geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
        guard let address = response?.firstResult(),
              let lines = address.lines else { return }

        self.placesLabel.text = lines.joined(separator: "\n")

        UIView.animate(withDuration: 0.25) {
//            self.pinImageVerticalConstraint.constant = (labelHeight - topInset) * 0.5
            self.view.layoutIfNeeded()
        }
      }
    }
}

extension GoogleViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
      guard status == .authorizedWhenInUse else { return }
        
        locationManager.requestLocation()
        
        googleMap.isMyLocationEnabled = true
        googleMap.settings.myLocationButton = true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
      guard let location = locations.first else {
        return
      }
      
        googleMap.camera = GMSCameraPosition(
        target: location.coordinate,
        zoom: 15,
        bearing: 0,
        viewingAngle: 0)
//      fetchPlaces(near: location.coordinate)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
      print(error)
    }
}

extension GoogleViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
      reverseGeocode(coordinate: position.target)
    }
}

// swiftlint:enable unused_closure_parameter
