//
//  AppleViewController+MapView.swift
//  DiscoveryMap
//
//  Created by Antony Alexander Mylvaganam on 1/7/22.
//

import Foundation
import MapKit

extension AppleViewController: MKMapViewDelegate {
    
//    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
//        guard let location = userLocation.location else { return }
//        let region = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: 2000, longitudinalMeters: 2000)
//    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
    }
    
    func mapViewDidChangeVisibleRegion(_ mapView: MKMapView) {
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        search()
    }
    
    func mapView(_ mapView: MKMapView, markerAnnotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        guard let annotation = view.annotation else { return }
        annotation.openInMap()
//        annotation.openInDialer()
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        guard let annotation = annotation as? ConfigurableAnnotation else { return nil }
        
        guard let view = mapView.dequeueReusableAnnotationView(withIdentifier: annotation.identifier) else {
            let markerAnnotationView: MKMarkerAnnotationView
            switch annotation.type {
            case .search:
                markerAnnotationView = SearchAnnotationView(annotation: annotation, reuseIdentifier: annotation.identifier)
                
            case .places:
                markerAnnotationView = PlacesAnnotationView(annotation: annotation, reuseIdentifier: annotation.identifier)
            }
            let detailedCalloutView = DetailedCalloutView.fromXib()
            detailedCalloutView.configure(annotation: annotation)
            markerAnnotationView.detailCalloutAccessoryView = detailedCalloutView
            markerAnnotationView.canShowCallout = true
            return markerAnnotationView
        }
        let callout = view.detailCalloutAccessoryView as? DetailedCalloutView
        callout?.configure(annotation: annotation)
        return view
    }
}

extension AppleViewController: LocationServiceDelegate {
    func didFail(with error: LocationServices.LocationError) {
        print(error)
    }
    
    func didFetch(location: CLLocation) {
        print(location)
    }
}
