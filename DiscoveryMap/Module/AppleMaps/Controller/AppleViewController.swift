//
//  AppleViewController.swift
//  DiscoveryMap
//
//  Created by Antony Alexander Mylvaganam on 1/5/22.
//

import CoreLocation
import MapKit
import UIKit

class AppleViewController: UIViewController {
    
    @IBOutlet private weak var appleMap: MKMapView! {
        didSet {
            appleMap.delegate = self
            appleMap.register(DetailedCalloutView.self,
                              forAnnotationViewWithReuseIdentifier: "DetailedCalloutView")
        }
    }
    
    @IBOutlet private weak var appleMapSearch: UISearchBar! {
        didSet {
            appleMapSearch.delegate = self
        }
    }
    
    private var userLocation = LocationServices(locationProvider: CLLocationManager())
    
    private var annotations = [ConfigurableAnnotation]()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupLocationAccess()
        loadStates()
    }
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        userLocation.delegate = self
        userLocation.stopLocationUpdates()
    }
    
    private func setupLocationAccess() {
        userLocation.delegate = self
        userLocation.fetchUserLocation()
    }
    
    private func loadStates() {
        
        guard let jsonObjUrl = Bundle.main.url(forResource: "USStates", withExtension: "json"),
              let stateData = try? Data(contentsOf: jsonObjUrl),
              let jsonObj = try? JSONSerialization.jsonObject(with: stateData,
                                                              options: .fragmentsAllowed) as? [[String: Any]] else { return }
        
        self.annotations.append(contentsOf: jsonObj.compactMap { dict in
            guard let stateName = dict["state"] as? String,
                  let statelatitude = dict["latitude"] as? Double,
                  let statelongitude = dict["longitude"] as? Double else { return nil }
            
            guard let stateAbrvFirst = stateName.first,
                  let stateAbrvSecond = stateName.last else { return nil }
            
            let stateAbrv = "\(stateAbrvFirst.uppercased())\(stateAbrvSecond.uppercased())"
            
            return PlacesAnnotation(coordinate: CLLocationCoordinate2D(latitude: statelatitude, longitude: statelongitude), title: stateAbrv, subtitle: stateName)
        })
        appleMap.addAnnotations(self.annotations)
        appleMap.showAnnotations(self.annotations, animated: true)
    }
    
    func performLocalSearch(_ searchString: String) {
        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = searchString
        request.region = appleMap.region
        
        let search = MKLocalSearch(request: request)
        search.start { [weak self] response, _ in
            guard let self = self else { return }
            self.appleMap.removeAnnotations(self.appleMap.annotations)
            if let result = response?.mapItems {
                for item in result {
                    let annotation = SearchAnnotation(coordinate: item.placemark.coordinate,
                                                      title: item.name ?? "",
                                                      subtitle: item.placemark.formattedAddress,
                                                      phone: item.phoneNumber ?? "",
                                                      zipCode: item.placemark.locality ?? "")
                    self.annotations.append(annotation)
                    self.appleMap.addAnnotations(self.annotations)
//                    self.appleMap.showAnnotations(annotations, animated: true)
                }
            }
        }
    }
}

extension AppleViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        search()
    }
    func search() {
        guard let searchText = appleMapSearch.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), !searchText.isEmpty else { return }
        performLocalSearch(searchText)
    }
}
