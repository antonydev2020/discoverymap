//
//  DetailedCalloutView.swift
//  DiscoveryMap
//
//  Created by Antony Alexander Mylvaganam on 1/13/22.
//

import Foundation
import MapKit
import UIKit

class PlacesAnnotationView: MKMarkerAnnotationView {}

class DetailedCalloutView: UIView, ReusableUIView {
    
    @IBOutlet private weak var addressLabel: UILabel!
    @IBOutlet private weak var phoneLabel: UILabel!
    @IBOutlet private weak var zipCodeLabel: UILabel!
    
    func configure(annotation: ConfigurableAnnotation) {
        addressLabel.text = annotation.info
        phoneLabel.text = annotation.phone
        zipCodeLabel.text = annotation.zipCode
    }
    
    static func fromXib() -> DetailedCalloutView {
        guard let view = Bundle.main.loadNibNamed("DetailedCalloutView", owner: nil, options: nil)?.first as? DetailedCalloutView else { return DetailedCalloutView() }
        return view
    }
}
