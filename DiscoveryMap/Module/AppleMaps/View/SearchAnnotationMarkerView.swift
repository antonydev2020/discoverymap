//
//  SearchAnnotationMarkerView.swift
//  DiscoveryMap
//
//  Created by Antony Alexander Mylvaganam on 1/12/22.
//

import MapKit
import UIKit

class SearchAnnotationView: MKMarkerAnnotationView {}

class SearchAnnotationMarkerView: UIView, ReusableUIView {
    
    @IBOutlet private weak var searchNameLable: UILabel!
    @IBOutlet private weak var searchAddressLabel: UILabel!
    @IBOutlet private weak var searchPhoneLabel: UILabel!
    @IBOutlet private weak var searchPostalLabel: UILabel!
    
    func searchConfigure(name: String, address: String, phone: String, postal: String) {
        self.searchNameLable.text = name
        self.searchAddressLabel.text = address
        self.searchPhoneLabel.text = phone
        self.searchPostalLabel.text = postal
    }
    
    static func fromXib() -> SearchAnnotationMarkerView {
        guard let view = Bundle.main.loadNibNamed("SearchAnnotationMarkerView", owner: nil, options: nil)?.first as? SearchAnnotationMarkerView else { return SearchAnnotationMarkerView() }
        return view
    }
}
