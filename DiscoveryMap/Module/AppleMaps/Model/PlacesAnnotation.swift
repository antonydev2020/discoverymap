//
//  PlacesAnnotation.swift
//  DiscoveryMap
//
//  Created by Antony Alexander Mylvaganam on 1/7/22.
//

import Foundation
import MapKit

class SearchAnnotation: NSObject, ConfigurableAnnotation {
    
    let type: AnnotationType = .search
    var identifier: String = "SearchAnnotationMarkView"
    var coordinate: CLLocationCoordinate2D

    var title: String?
    var subtitle: String?
    var phone: String?
    var zipCode: String?
    
    var info: String?
    var image: String?
    
    init(coordinate: CLLocationCoordinate2D, title: String, subtitle: String, phone: String, zipCode: String) {
        self.coordinate = coordinate
        self.title = title
        self.info = subtitle
        self.phone = phone
        self.zipCode = zipCode
    }
}

class PlacesAnnotation: NSObject, ConfigurableAnnotation {
    
    var type: AnnotationType = .places
    var identifier: String = "PlaceAnnotationsMarkerView"
    var coordinate: CLLocationCoordinate2D
    
    var title: String?
    var subtitle: String?
    
    var info: String?
    var phone: String?
    var image: String?
    var zipCode: String?
    
    init(coordinate: CLLocationCoordinate2D, title: String, subtitle: String) {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
    }
}
